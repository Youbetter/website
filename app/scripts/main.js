'use strict';

var VIMEO_ACCESS_TOKEN = '8ca4ac5d3fb49be34f9c018588caee93',
    VIMEO_VIDEOS_PER_PAGE = 50,
    VIMEO_API_BASE_URL = 'https://api.vimeo.com',

    BACKGROUND_VIDEO_WIDTH = 635,
    BACKGROUND_VIDEO_HEIGHT = 264,

    userAgent = navigator.userAgent.toLowerCase(),
    isMobileDevice = /android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(userAgent),

    $window = $(window),
    $videosContainer = $('.videos'),
    $header = $('header'),
    $headerBurger = $('.burger'),
    $headerLogo = $('#logo'),
    $headerNav = $('nav'),
    $backgroundVideo = $('video'),
    $videoPlayer = $('.player');

$videoPlayer.on('click', hidePlayer);

if (isMobileDevice) {
  $headerBurger.on('click', toggleHeaderNav);
} else {
  $header.addClass('nav-on-hover');
}

if ($videosContainer.length) {
  var loader = createLoader($videosContainer),
      videoImageQueue = [];

  $.when(
    getVimeoVideos(),
    getVimeoMappings()
  ).then(function(videos, vimeoMappings) {
    _.each(videos, buildVideo.bind(null, vimeoMappings, videoImageQueue));
    removeLoader(loader);
    resolveVideoImageQueue(videoImageQueue);
  });
}

if (!isMobileDevice && $backgroundVideo.length) {
  $backgroundVideo
    .coverVid(BACKGROUND_VIDEO_WIDTH, BACKGROUND_VIDEO_HEIGHT);

  $backgroundVideo.get(0).addEventListener('loadeddata', function() {
    $backgroundVideo.addClass('visible')
      .parent()
      .addClass('background-playing');
  });
} else {
  $backgroundVideo.hide();
}

function createLoader(container, opts) {
  var loader = container.spin(_.extend({
    lines: 9,
    length: 0,
    width: 5,
    radius: 32,
    corners: 1,
    rotate: 0,
    direction: 1,
    color: '#FFF',
    speed: 1,
    trail: 100,
    hwaccel: true,
    top: '100px'
  }, opts)).find('.spinner');

  getComputedStyle(loader.get(0)).opacity;
  loader.addClass('visible');

  return loader;
}

function transitionEndPolyfill(element) {
  var deferred = $.Deferred();

  transitionEnd(element).bind(function() {
    transitionEnd(element).unbind();
    deferred.resolve();
  });

  return deferred.promise();
}

function removeLoader(loader) {
  loader.removeClass('visible');
  return transitionEndPolyfill(loader).then(function() {
    loader.remove();
  });
}

function toggleHeaderNav() {
  $headerNav.toggleClass('visible');
  $headerLogo.toggleClass('hide-logo');
  $headerBurger.toggleClass('close');
}

function getVimeoMappings() {
  return $.ajax({
    cache: false,
    url: 'videos/mappings.json'
  }).then(function(result) {
    return result;
  });
}

function getVimeoVideos(page, perPage) {
  return $.ajax({
    url: VIMEO_API_BASE_URL + '/me/videos',
    data: {
      page: page || 1,
      access_token: VIMEO_ACCESS_TOKEN,
      per_page: perPage || VIMEO_VIDEOS_PER_PAGE
    }
  }).then(function(response) {
    return _.filter(response.data, function(video) {
      return video.privacy.view === 'anybody';
    });
  });
}

function loadVideoImage($videoContainer, $videoImage, video) {
  var deferred = $.Deferred(),
      image = _.last(video.pictures.sizes),
      imageSrc = image.link,
      $imageElement = $('<img />').attr({
        src: imageSrc
      });

  $imageElement.load(function() {
    $videoImage.css({
      backgroundImage: 'url(' + imageSrc + ')'
    });
    $videoContainer.addClass('visible');
    deferred.resolve();
  });

  return deferred.promise();
}

function resolveVideoImageQueue(videoImageQueue, counter) {
  counter = counter || 0;
  var promise = videoImageQueue[counter];

  if (!promise) { return $.when(); }

  return promise().then(function() {
    return resolveVideoImageQueue(videoImageQueue, ++counter);
  });
}

function buildVideo(vimeoMappings, videoImageQueue, video) {
  var $videoContainer = $('<div class="col-sm-6 col-md-4 video"></div>'),
      $videoImage = $('<div class="video-image"></div>'),
      $videoOverlay = $('<div class="video-overlay"></div>'),
      vimeoMapping = _.find(vimeoMappings, function(mapping) {
        return _.endsWith(video.uri, mapping.videoId);
      }),
      videoType = 'Imagefilm',
      videoName = video.name;

  if (vimeoMapping) {
    videoType = vimeoMapping.type;
    videoName = vimeoMapping.name;
  }

  videoImageQueue.push(loadVideoImage.bind(null, $videoContainer, $videoImage, video));

  $videoOverlay.append([
    '<div class="name">' + videoName + '</div>',
    '<div class="type">' + videoType + '</div>'
  ]);

  $videoContainer.append([
    $videoImage,
    $videoOverlay
  ]).appendTo($videosContainer).on('click', openVideo.bind(null, null, video));
}

function hidePlayer(evt) {
  if (evt && !$(evt.target).is($videoPlayer)) { return; }

  $window.off('.video');
  $videoPlayer.removeClass('visible');
  transitionEndPolyfill($videoPlayer).then(function() {
    $videoPlayer.hide().find('iframe').remove();
  });
}

function openVideo(evt, video) {
  $videoPlayer.show();
  getComputedStyle($videoPlayer.get(0)).display;

  var iframe = $('<iframe></iframe>').attr({
    frameborder: 0,
    src: 'https://player.vimeo.com' + video.uri.replace('videos', 'video') + '?title=0&byline=0&portrait=0&badge=0&autoplay=1&autopause=0',
    width: video.width,
    height: video.height
  });

  $videoPlayer.addClass('visible').append(iframe);
  transitionEndPolyfill($videoPlayer).then(function() {
    $window.on('keydown.video', function(evt) {
      if ((evt.keyCode || evt.which) === 27) {
        hidePlayer();
      }
    });
  });
}
